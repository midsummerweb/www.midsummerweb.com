
const gulp = require('gulp'),
  sass = require('gulp-sass'),
  autoprefixer = require('gulp-autoprefixer'),
  cssnano = require('gulp-cssnano'),
  eslint = require('gulp-eslint'),
  uglify = require('gulp-uglify'),
  imagemin = require('gulp-imagemin'),
  rename = require('gulp-rename'),
  concat = require('gulp-concat'),
  cache = require('gulp-cache'),
  livereload = require('gulp-livereload'),
  del = require('del'),
  rev = require('gulp-rev'),
  revReplace = require("gulp-rev-replace"),
  babel = require('gulp-babel'),
  sourcemaps = require('gulp-sourcemaps')

gulp.task('images', () => gulp
  .src('src/img/**/*')
  .pipe(cache(imagemin({ optimizationLevel: 5, progressive: true, interlaced: true })))
  .pipe(gulp.dest('dist/img'))
)

gulp.task('revimages', ['images'], () => gulp
  .src('src/img/**/*')
  .pipe(rev())
  .pipe(gulp.dest('dist'))
  .pipe(rev.manifest())
  .pipe(gulp.dest('dist'))
)

gulp.task('styles', [ 'revimages' ], () => gulp
  .src('src/style/main.scss')
  .pipe(revReplace({ manifest: gulp.src('dist/rev-manifest.json') }))
  .pipe(sass())
  .pipe(autoprefixer('last 2 version'))
  .pipe(cssnano())
  .pipe(gulp.dest('dist/style'))
)

gulp.task('lint', () => gulp
  .src('src/script/**/*.js')
  .pipe(eslint())
  .pipe(eslint.format())
  .pipe(eslint.failAfterError())
)

gulp.task('scripts', () => gulp
  .src('src/script/**/*.js')
  .pipe(sourcemaps.init())
  .pipe(babel())
  .pipe(concat('main.js'))
  .pipe(uglify())
  .pipe(sourcemaps.write('.'))
  .pipe(gulp.dest('dist/script'))
)

gulp.task('revision', ['scripts', 'styles'], () => gulp
  .src(['dist/**/*.css', 'dist/**/*.js'])
  .pipe(rev())
  .pipe(gulp.dest('dist'))
  .pipe(rev.manifest({ merge: true }))
  .pipe(gulp.dest('dist'))
)

gulp.task('html', ['revision'], () => gulp
  .src('src/index.html')
  .pipe(revReplace({ manifest: gulp.src('dist/rev-manifest.json') }))
  .pipe(gulp.dest('dist'))
);

gulp.task('clean', () => del(['dist/style', 'dist/script', 'dist/img']))

gulp.task('default',
  ['lint', 'clean'],
  () => gulp.start('styles', 'scripts', 'images', 'html')
)

gulp.task('watch', () => {
  gulp.watch('src/style/**/*.scss', ['styles'])
  gulp.watch('src/script/**/*.js', ['scripts'])
  gulp.watch('src/img/**/*', ['images'])
  livereload.listen()
  gulp.watch(['dist/**']).on('change', livereload.changed)
})
